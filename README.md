# ROBOTFINDSKITTEN

You are robot.

You must find kitten.

Problem is, you don't know what kitten looks like.

![](rfk.png)

Play [in the browser!](http://tic.computer/play?cart=1093)

Written in [Fennel](https://fennel-lang.org) for the
[TIC-80](https://tic.computer) platform

[Original game](http://robotfindskitten.org) by
[Leonard Richardson](https://www.crummy.com/); see
[this talk on the background of the game](https://www.crummy.com/writing/speaking/2017-Roguelike%20Celebration/).

## License

Copyright © 2020 Phil Hagelberg

Distributed under the GNU General Public License version 3 or later.
